import telegram_helpers as tg
import datetime

import parse_helpers as parse

now = datetime.datetime.now()
year = str(now.year)

#Test data
#now = datetime.datetime.strptime(f"12 August 2021", '%d %B %Y')
#year = 2021

craces = parse.get_collect_races(year, now)
cnext_races = parse.get_next_races(craces)

if abs(cnext_races[0]['RDelta']) == 3:
    tg.send(parse.next_races_call(cnext_races))
