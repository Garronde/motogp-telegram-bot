# MotoGp Telegram bot

## prereq

- lib install:

        pip3 install pyTelegramBotAPI
        pip3 install beautifulsoup4

- create a Telegram bot:

https://core.telegram.org/bots

- get the chat ID:

    - chat with your bot
    - https://api.telegram.org/botYOURTOKEN/getUpdates
    - Look for your chatID



- create a file personal_helpers.py in it:

        def get_bot_token():   
            return 'yourtoken'
        def get_chat_id():
            return 'yourchatid'

- add in crontab:

`0 11 * * * python3 /home/pi/motogp-telegram-bot/cron_motogp.py`

`0 9  * * * /sbin/shutdown -r now`

- add in /etc/rc.local:

`sudo -H -u pi /usr/bin/python3 /home/pi/motogp-telegram-bot/motogp-bot.py &`


## run
- simple:

        python3 motogp-bot.py

- background:

        nohup python3 motogp-bot.py &
        
- at startup: (in /etc/rc.local)
        
        sudo -H -u pi /usr/bin/python3 /home/pi/motogp-telegram-bot/motogp-bot.py &
