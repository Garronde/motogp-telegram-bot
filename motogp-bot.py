import telebot
import datetime

import parse_helpers as parse
import personal_helpers as perso

import telegram_helpers as tg

# You need to create a file personal_helpers.py in it :
#def get_bot_token():
#    return 'yourtoken'
#def get_chat_id():
#    return 'yourchatid'
bot_token = perso.get_bot_token()
bot_chatID = perso.get_chat_id()

#Creating the bot
bot = telebot.TeleBot(bot_token)

now = datetime.datetime.now()
year = str(now.year)

#Test data
#now = datetime.datetime.strptime(f"14 August 2021", '%d %B %Y')
#year = 2021


@bot.message_handler(commands=['start'])
def handle_command(message):
    bot.send_message(bot_chatID,"Hello I'm a telgram bot created by garronde, I can get info about current MotoGP championship. \nUse /help if you need some.")
    
@bot.message_handler(commands=['help'])
def handle_command(message):
    bot.send_message(bot_chatID,"List of available commands:\n/calendar - displays the current MotoGP championship calendar\n/nextrace - details about the next race\n/riders - display the riders' standing\n/constructors - display the constructors' standing")
  
@bot.message_handler(commands=['calendar'])
def handle_command(message):
    bot.send_message(bot_chatID,parse.calendar_call(year))

@bot.message_handler(commands=['nextrace'])
def handle_command(message):
    vraces = parse.get_collect_races(year, now)
    vnext_races = parse.get_next_races(vraces)
    bot.send_message(bot_chatID,parse.next_races_call(vnext_races))
  
@bot.message_handler(commands=['calendar'])
def handle_command(message):
    bot.send_message(bot_chatID,parse.calendar_call(year))

@bot.message_handler(commands=['riders'])
def handle_command(message):
    bot.send_message(bot_chatID,parse.rider_champ_call(year))

@bot.message_handler(commands=['constructors'])
def handle_command(message):
    bot.send_message(bot_chatID,parse.contructor_champ_call(year))

bot.polling()