import requests
from bs4 import BeautifulSoup
import datetime

#import telegram_helpers as tg

from operator import itemgetter

def get_collect_races(year, now):

	url = f'https://results.motorsportstats.com/series/motogp/season/{year}'
	page = requests.get(url)
	soup = BeautifulSoup(page.content, 'html.parser')
	#print(soup.body.prettify())

	collected_races_list = []

	races_section = soup.find_all(class_='Vv8Fg')[2]
	races = races_section.find_all(class_="_3AoAU")

	for race in races:
		#print(datetime.datetime.strptime(race['RDate'], '%d %B %Y').strftime('%Y-%m-%d'))
		if race.find_all(class_='_2sWDi f4AjL')[0].text == 'TBC':
			thisRDate = datetime.datetime.strptime(f"31 December {year}", '%d %B %Y')
		else:	
			thisRDate = datetime.datetime.strptime(race.find_all(class_='_2sWDi f4AjL')[0].text, '%d %B %Y')
		thisRName = race.find_all(class_='_2sWDi f4AjL')[1].text
		thisRTrack = race.find_all(class_='_2sWDi f4AjL')[2].text
		if race.find_all(class_='_2sWDi f4AjL')[3].text == '':
			thisRWinner = 'TBA'
		else:
			thisRWinner = race.find_all(class_='_2sWDi f4AjL')[3].text

		delta = now - thisRDate
		
		
		collected_races_list.append({'RDate':thisRDate,'RName':thisRName,'RTrack':thisRTrack,'RWinner':thisRWinner,'RDelta':delta.days})


	return collected_races_list

def get_next_races(collected_races):
	
	next_races_list = []

	for race in collected_races:
		if race['RDelta'] <= 0:
			next_races_list.append(race)

	next_races = sorted(next_races_list, key=itemgetter('RDelta'), reverse=True)

	return next_races		

def next_races_call(next_races):

	next_races_txt = ''

	if abs(next_races[0]['RDelta']) > 1:
		next_races_txt += "-"*50 + f"\nNext race in {abs(next_races[0]['RDelta'])} days" + "\n" + "-"*50
		next_races_txt +=(f"\nGP Name: {next_races[0]['RName']}")
		next_races_txt +=(f"\nGP Track: {next_races[0]['RTrack']}")
		next_races_txt += "\n"+"-"*50+"\n"
	elif abs(next_races[0]['RDelta']) == 1:
		next_races_txt += "-"*50 + f"\nNext race in {abs(next_races[0]['RDelta'])} day" + "\n" + "-"*50
		next_races_txt +=(f"\nGP Name: {next_races[0]['RName']}")
		next_races_txt +=(f"\nGP Track: {next_races[0]['RTrack']}")
		next_races_txt += "\n"+"-"*50+"\n"
	elif abs(next_races[0]['RDelta']) == 0:
		next_races_txt += "-"*50 + f"\nToday is race day!" + "\n" + "-"*50
		next_races_txt +=(f"\nGP Name: {next_races[0]['RName']}")
		next_races_txt +=(f"\nGP Track: {next_races[0]['RTrack']}")
		next_races_txt += "\n" + "-"*50 + f"\nThe next race after that is in {abs(next_races[1]['RDelta'])} days" + "\n" + "-"*50
		next_races_txt +=(f"\nGP Name: {next_races[1]['RName']}")
		next_races_txt +=(f"\nGP Track: {next_races[1]['RTrack']}")
		next_races_txt += "\n"+"-"*50+"\n"
	
	return next_races_txt


def calendar_call(year):
	
	url = f'https://results.motorsportstats.com/series/motogp/season/{year}'
	page = requests.get(url)
	soup = BeautifulSoup(page.content, 'html.parser')
	#print(soup.body.prettify())

	calendar_txt = ""

	calendar_section = soup.find_all(class_='Vv8Fg')[2]
	calendar_txt += "-"*50 + f"\nMoto GP {year} {calendar_section.h2.string}" + "\n" + "-"*50 +"\n"

	cal = calendar_section.find_all(class_="_3AoAU")

	for event in cal:
		#print(event.prettify())
		calendar_txt +=(f"GP Date: {event.find_all(class_='_2sWDi f4AjL')[0].text}")
		calendar_txt +=(f"\nGP Name: {event.find_all(class_='_2sWDi f4AjL')[1].text}")
		calendar_txt +=(f"\nGP Track: {event.find_all(class_='_2sWDi f4AjL')[2].text}")
		if event.find_all(class_='_2sWDi f4AjL')[3].text == '':
			calendar_txt +=(f"\nGP Winner: TBA\n")
		else:
			calendar_txt +=(f"\nGP Winner: {event.find_all(class_='_2sWDi f4AjL')[3].text}\n")
		calendar_txt += "-"*50+"\n"


	return calendar_txt

def rider_champ_call(year):
	
	url = f'https://results.motorsportstats.com/series/motogp/season/{year}'
	page = requests.get(url)
	soup = BeautifulSoup(page.content, 'html.parser')
	#print(soup.body.prettify())

	rider_txt = ''

	championship_section = soup.find_all(class_='Vv8Fg')[4]
	rider_txt += "-"*50 + f"\n{championship_section.h2.string} (top5)" + "\n" +"-"*50+"\n"
	#print(championship_section.prettify())
	champions = championship_section.find_all(class_="_3AoAU")
	if len(champions) == 0:
		rider_txt += (f"No points delivered yet")
		rider_txt +=  "\n"+"-"*50+"\n"

	else:
		for champ in champions:
			#print(champ.prettify())
			rider_txt += (f"Rider name: {champ.find_all(class_='_2sWDi f4AjL')[0].text}")
			rider_txt += "\n" + (f"Rider points: {champ.find_all(class_='_2sWDi JPoo2')[0].text}")
			rider_txt +=  "\n"+"-"*50+"\n"

	return rider_txt

def contructor_champ_call(year):

	url = f'https://results.motorsportstats.com/series/motogp/season/{year}'
	page = requests.get(url)
	soup = BeautifulSoup(page.content, 'html.parser')
	#print(soup.body.prettify())

	constructor_txt = ''

	constructor_championship_section = soup.find_all(class_='Vv8Fg')[5]
	constructor_txt += "-"*50 + f"\n{constructor_championship_section.h2.string} (top5)" + "\n" +"-"*50+"\n"
	#print(constructor_championship_section.prettify())
	constructor_champions  = constructor_championship_section.find_all(class_="_3AoAU")
	#print(len(champions))
	if len(constructor_champions) == 0:
			constructor_txt += (f"No points delivered yet")
	else:
		for champ in constructor_champions :
			#print(champ.prettify())
			constructor_txt += (f"Constructor: {champ.find_all(class_='_2sWDi f4AjL')[0].text}")
			constructor_txt += "\n" + (f"Constructor points: {champ.find_all(class_='_2sWDi JPoo2')[0].text}")
			constructor_txt +=  "\n"+"-"*50+"\n"

	return constructor_txt
